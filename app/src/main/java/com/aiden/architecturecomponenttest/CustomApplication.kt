package com.aiden.architecturecomponenttest

import android.app.Application

class CustomApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

    companion object {
        lateinit var INSTANCE: CustomApplication
            private set
    }
}