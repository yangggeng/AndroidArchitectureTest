package com.aiden.architecturecomponenttest.data.service

import android.arch.lifecycle.LiveData
import com.aiden.architecturecomponenttest.entity.HospitalBean
import com.aiden.architecturecomponenttest.http.HttpResponse
import retrofit2.http.GET

interface HospitalService {

    @GET("4/news/latest")
    fun getHospitalList(): LiveData<HttpResponse<HospitalBean>>
}