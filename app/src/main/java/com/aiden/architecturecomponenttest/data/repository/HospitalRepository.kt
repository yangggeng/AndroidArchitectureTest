package com.aiden.architecturecomponenttest.data.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import com.aiden.architecturecomponenttest.data.datasource.hospital.HospitalDataSource
import com.aiden.architecturecomponenttest.entity.HospitalBean

class HospitalRepository private constructor(var hospitalRemoteDataSource: HospitalDataSource, var hospitalLocalDataSource: HospitalDataSource) : HospitalDataSource {

    companion object {
        var instance: HospitalRepository? = null

        fun getInstance(hospitalRemoteDataSource: HospitalDataSource, hospitalLocalDataSource: HospitalDataSource): HospitalRepository? {
            if (instance == null) {
                synchronized(HospitalRepository::class) {
                    if (instance == null) {
                        instance = HospitalRepository(hospitalRemoteDataSource = hospitalRemoteDataSource, hospitalLocalDataSource = hospitalLocalDataSource)
                    }
                }
            }
            return instance
        }
    }

    override fun getHospitalList(): LiveData<HospitalBean> {

    }
}