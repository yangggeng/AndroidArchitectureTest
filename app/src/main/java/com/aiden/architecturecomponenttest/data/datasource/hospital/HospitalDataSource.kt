package com.aiden.architecturecomponenttest.data.datasource.hospital

import android.arch.lifecycle.LiveData
import com.aiden.architecturecomponenttest.entity.HospitalBean

interface HospitalDataSource {

    fun getHospitalList(): LiveData<HospitalBean>
}