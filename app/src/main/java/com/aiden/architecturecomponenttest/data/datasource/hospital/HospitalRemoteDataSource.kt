package com.aiden.architecturecomponenttest.data.datasource.hospital

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import com.aiden.architecturecomponenttest.entity.HospitalBean
import com.aiden.architecturecomponenttest.http.NetWork

class HospitalRemoteDataSource: HospitalDataSource {
    override fun getHospitalList(): LiveData<HospitalBean> {
        val response = NetWork.INSTANCE!!.hospitalService().getHospitalList()
        return Transformations.map(response) { input -> input!!.data }
    }
}