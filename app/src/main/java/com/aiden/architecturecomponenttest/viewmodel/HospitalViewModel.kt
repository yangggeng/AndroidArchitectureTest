package com.aiden.architecturecomponenttest.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.aiden.architecturecomponenttest.entity.HospitalBean

class HospitalViewModel: ViewModel() {

    var hospitalData: LiveData<HospitalBean> = MutableLiveData<HospitalBean>()

}