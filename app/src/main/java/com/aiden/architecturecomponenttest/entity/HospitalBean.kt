package com.aiden.architecturecomponenttest.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "hospital")
data class HospitalBean(
        @PrimaryKey(autoGenerate = true)
        var id: Int,
        var addr: String, var bus: String, var cityName: String, @ColumnInfo var hosName: String, var img: String, var info: String, var provinceName: String, var tsks: String, var keshi: String, var zzjb: String) : Serializable
