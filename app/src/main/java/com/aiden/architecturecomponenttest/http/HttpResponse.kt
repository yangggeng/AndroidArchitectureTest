package com.aiden.architecturecomponenttest.http

data class HttpResponse<T>(var code: Int, var msg: String, var data: T)