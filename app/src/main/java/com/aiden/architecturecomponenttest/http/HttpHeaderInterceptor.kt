package com.aiden.architecturecomponenttest.http

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response

class HttpHeaderInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val oldRequest = chain!!.request()

        // 添加公共参数到url中
        val url: HttpUrl = oldRequest.url().newBuilder().addQueryParameter("", "").build()

        // 添加Header
        val requestBuilder = oldRequest.newBuilder().addHeader("Authorization", "token or other").url(url)

        return chain.proceed(requestBuilder.build())
    }
}